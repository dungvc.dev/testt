//
//  MusicPlayer.swift
//  TestInmobile
//
//  Created by Công Dũng on 27/05/2024.
//

import SwiftUI

import Foundation
import AVFoundation
import Combine

class MusicPlayerViewModel: ObservableObject {
    private var audioPlayer: AVAudioPlayer?
    private var timer: AnyCancellable?

    @Published var currentTime: Double = 0.0
    @Published var duration: Double = 0.0
    @Published var isPlaying = false

    var currentTimeString: String {
        let minutes = Int(currentTime) / 60
        let seconds = Int(currentTime) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }

    var durationString: String {
        let minutes = Int(duration) / 60
        let seconds = Int(duration) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }

    func setupAudioPlayer() {
        if let path = Bundle.main.path(forResource: "beat", ofType: "mp3") {
            let url = URL(fileURLWithPath: path)
            do {
                audioPlayer = try AVAudioPlayer(contentsOf: url)
                duration = audioPlayer?.duration ?? 0.0
            } catch {
                print("Error loading audio file")
            }
        }
    }

    func play() {
        audioPlayer?.play()
        startTimer()
        self.isPlaying = true
    }

    func pause() {
        audioPlayer?.pause()
        stopTimer()
        self.isPlaying = false
    }


    func seekToTime() {
        audioPlayer?.currentTime = currentTime
    }

    private func startTimer() {
        timer = Timer.publish(every: 1.0, on: .main, in: .common).autoconnect()
            .sink { [weak self] _ in
                self?.updateCurrentTime()
                guard let isPlaying = self?.audioPlayer?.isPlaying else { return }
                self?.isPlaying = isPlaying
            }
    }

    private func stopTimer() {
        timer?.cancel()
    }

    private func updateCurrentTime() {
        currentTime = audioPlayer?.currentTime ?? 0.0
    }
}
