//
//  MusicPlayerView.swift
//  TestInmobile
//
//  Created by Công Dũng on 27/05/2024.
//

import SwiftUI
import AVFoundation

struct MusicPlayerView: View {
    @StateObject private var viewModel = MusicPlayerViewModel()

    var body: some View {
        VStack {
            Text("Về Đâu Mái Tóc Người Thương")
                .scaledToFill()
                .font(.title)
                .padding(.top, 48)

            Spacer()

            LyricView(timer: $viewModel.currentTime)

            Spacer()
            VStack {
                if viewModel.isPlaying {
                    Button(action: {
                        viewModel.pause()
                    }) {
                        Image(systemName: "pause.fill")
                            .resizable()
                            .frame(width: 32, height: 32)
                    }
                    .padding()
                } else {
                    Button(action: {
                        viewModel.play()
                    }) {
                        Image(systemName: "play.fill")
                            .resizable()
                            .frame(width: 32, height: 32)
                    }
                    .padding()
                }
            }

            Slider(
                value: $viewModel.currentTime,
                in: 0...viewModel.duration,
                onEditingChanged: { _ in
                    viewModel.seekToTime()
                }
            )
            HStack {
                Text(viewModel.currentTimeString)
                Spacer()
                Text(viewModel.durationString)
            }
        }
        .padding()
        .onAppear {
            viewModel.setupAudioPlayer()
        }
    }
}

#Preview {
    MusicPlayerView()
}
