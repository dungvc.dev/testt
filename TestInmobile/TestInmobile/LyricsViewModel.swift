//
//  XMLLyricParser.swift
//  TestInmobile
//
//  Created by Công Dũng on 27/05/2024.
//

import Foundation

class LyricsViewModel: NSObject, XMLParserDelegate, ObservableObject {
    @Published var lyricTimes: [String: Double] = [:]
    @Published var params: [Param] = []
    @Published var lyricsList = [Lyric]()
    private var currentParam: Param?
    private var currentText = ""
    private var currentTime: Double?

    func parse(data: Data) {
        let parser = XMLParser(data: data)
        parser.delegate = self
        parser.parse()
    }

    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        if elementName == "param" {
            currentParam = Param()
        } else if elementName == "i" {
            if let vaString = attributeDict["va"], let va = Double(vaString) {
                currentTime = va
            }
        }
    }

    func parser(_ parser: XMLParser, foundCharacters string: String) {
        currentText += string
    }

    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "i" {
            if let time = currentTime {
                let segment = LyricSegment(time: time, text: currentText.trimmingCharacters(in: .whitespacesAndNewlines))
                currentParam?.segments.append(segment)
            }
            currentText = ""
            currentTime = nil
        } else if elementName == "param" {
            if let param = currentParam {
                params.append(param)
            }
            currentParam = nil
        }
    }

    func reloadData() {
        if let path = Bundle.main.path(forResource: "lyrics", ofType: "xml") {
            let url = URL(fileURLWithPath: path)
            if let data = try? Data(contentsOf: url) {
                parse(data: data)

                var lyricsListTemp: [Lyric] = []

                for (index, param) in params.enumerated() {
                    guard let firstSegmentTime = param.segments.first?.time,
                          let lastSegmentTime = param.segments.last?.time else {
                        continue
                    }

                    let segmentsText = param.segments.map { $0.text }
                    let joinedSegmentsText = segmentsText.joined(separator: " ")

                    let nextParamStartTime = index < params.count - 1 ? params[index + 1].segments.first?.time : lastSegmentTime

                    let lyric = Lyric(
                        text: joinedSegmentsText,
                        timeStart: firstSegmentTime,
                        timeEnd: nextParamStartTime ?? lastSegmentTime,
                        timeEndShow: lastSegmentTime
                    )

                    lyricsListTemp.append(lyric)
                }

                lyricsList = lyricsListTemp
            }
        }
    }
}
