//
//  TextLineView.swift
//  TestInmobile
//
//  Created by Công Dũng on 01/06/2024.
//

import SwiftUI

struct TextLineView: View {
    @State var width = 0.0
    @State var isHighlighted = false
    @State var aaa = false
    var scrollToID: UUID? = nil
    @Binding var timer: Double
    var lyric: Lyric

    init(
        width: Double = 0.0,
        isHighlighted: Bool = false,
        aaa: Bool = false,
        scrollToID: UUID? = nil,
        timer: Binding<Double>,
        lyric: Lyric
    ) {
        self.width = width
        self.isHighlighted = isHighlighted
        self.aaa = aaa
        self.scrollToID = scrollToID
        _timer = timer
        self.lyric = lyric
    }

    var textLineView: some View {
        VStack(alignment: .leading, spacing: 4) {
            Text(lyric.text)
                .font(.system(
                    size: 15,
                    weight: isHighlighted ? .heavy : .bold,
                    design: .default)
                )
                .foregroundColor(.gray)
                .multilineTextAlignment(.leading)
                .fixedSize(horizontal: true, vertical: false)
                .id(lyric.id)
        }
    }

    var body: some View {
        textLineView
            .overlay(
                GeometryReader { geo in
                    ZStack(alignment: .leading) {
                        Color.clear
                        Rectangle()
                            .fill(Color.blue)
                            .frame(width: width, alignment: .leading)
                            .onChange(of: timer) { time in
                                if time < (lyric.timeStart - 0.5) || time > (lyric.timeEnd - 0.5) {
                                    // make to animation hight light text
                                    //                                    isHighlighted = true
                                    width = 0.0
                                } else {
                                    withAnimation(.linear(duration: lyric.timeEndShow - lyric.timeStart - 0.5)) {
                                        // make to animation hight light text
                                        //                                        isHighlighted = false
                                        width = geo.size.width
                                    }
                                }

                            }
                    }
                    .mask(
                        textLineView
                    )
                }
            )
    }
}
