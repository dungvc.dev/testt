//
//  LyricView.swift
//  TestInmobile
//
//  Created by Công Dũng on 27/05/2024.
//

import SwiftUI

struct LyricView: View {
    @StateObject private var viewModel = LyricsViewModel()
    @State private var width = 0.0
    @State private var scrollToID: UUID? = nil
    @Binding var timer: Double
    @State var progress: CGFloat = 0.0
    @State var phase: CGFloat = 0.0
    @State var textWidth: CGFloat = 0.0

    init(timer: Binding<Double>) {
        _timer = timer
    }

    var body: some View {
        ScrollViewReader { proxy in
            ScrollView(showsIndicators: false) {
                VStack(alignment: .leading, spacing: 4) {
                    Image(systemName: "music.note")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 20, height: 20)
                        .fontWeight(scrollToID == nil ? .bold : .regular)
                        .foregroundColor(scrollToID == nil ? .blue : .gray)
                        .id(0)
                        .padding(.bottom, 16)
                    ForEach(viewModel.lyricsList) { lyric in
                        TextLineView(
                            width: width,
                            scrollToID: scrollToID,
                            timer: $timer,
                            lyric: lyric
                        )
                    }
                    .padding(.bottom, 20)
                }
            }
            .onChange(of: timer) { newTimer in
                scrollToSegment(forTime: newTimer, withProxy: proxy)
            }
            .frame(maxWidth: .infinity, maxHeight: 450)
            .onAppear {
                viewModel.reloadData()
            }
        }
    }

    private func scrollToSegment(forTime time: Double, withProxy proxy: ScrollViewProxy) {
        let line = viewModel.lyricsList.first { ($0.timeStart...$0.timeEnd).contains(time - 0.5) }
        if let line {
            scrollToID = line.id
        }

        if time < viewModel.lyricsList.first?.timeStart ?? 0.0 {
            scrollToID = nil
            withAnimation {
                proxy.scrollTo(0, anchor: .top)
            }
        }

        withAnimation {
            proxy.scrollTo(scrollToID, anchor: .top)
        }
    }
}
