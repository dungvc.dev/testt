//
//  Lyric.swift
//  TestInmobile
//
//  Created by Công Dũng on 27/05/2024.
//

import Foundation

// Entity
struct LyricSegment: Identifiable {
    let id = UUID()
    let time: Double
    let text: String
}

struct Param: Identifiable {
    let id = UUID()
    var segments: [LyricSegment] = []
}

// Model
struct Lyric: Identifiable {
    let id = UUID()
    let text: String
    let timeStart: Double
    let timeEnd: Double
    let timeEndShow: Double
}

