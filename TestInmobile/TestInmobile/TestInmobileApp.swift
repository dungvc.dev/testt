//
//  TestInmobileApp.swift
//  TestInmobile
//
//  Created by Công Dũng on 27/05/2024.
//

import SwiftUI

@main
struct TestInmobileApp: App {
    var body: some Scene {
        WindowGroup {
            MusicPlayerView()
        }
    }
}
